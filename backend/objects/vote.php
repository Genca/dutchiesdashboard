
<?php class Vote
{
    private $conn;
    private $table_name = "votes";

    public $pictureId;
    public $userId;
    public $vote;
    public $added;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function add()
    {
        $query = "INSERT INTO
                " . $this->table_name . "
            SET
            pictureId=:pictureId, userId=:userId, vote=:vote, added=:added";

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":pictureId", $this->pictureId);
        $stmt->bindParam(":userId", $this->userId);
        $stmt->bindParam(":vote", $this->vote);
        $stmt->bindParam(":added", $this->added);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    function getVotesForPicture()
    {

        $query = "SELECT
                v.pictureId, v.userId, v.vote, v.added
            FROM
                " . $this->table_name . " v
            WHERE
            v.pictureId = ?
            ORDER BY
                v.added DESC";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->pictureId);

        $stmt->execute();

        return $stmt;
    }
}
?>
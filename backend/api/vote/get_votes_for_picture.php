
<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

include_once '../../vendor/firebase/php-jwt/src/BeforeValidException.php';
include_once '../../vendor/firebase/php-jwt/src/ExpiredException.php';
include_once '../../vendor/firebase/php-jwt/src/SignatureInvalidException.php';
include_once '../../vendor/firebase/php-jwt/src/JWT.php';

include_once '../../config/database.php';
include_once '../../config/core.php';

include_once '../../objects/vote.php';

use \Firebase\JWT\JWT;

$database = new Database();
$db = $database->getConnection();

$vote = new Vote($db);

$data = json_decode(file_get_contents("php://input"));

$jwt = isset($data->jwt) ? $data->jwt : "";


if ($jwt) {

    try {
        //Used the JWT key of Google Firebase to decode ID Token of user
        // https://firebase.google.com/docs/auth/admin/verify-id-tokens
        $decoded = JWT::decode($jwt, $key, array('RS256'));

        $vote->pictureId = isset($_GET['pictureId']) ? $_GET['pictureId'] : "";

        if ($vote->pictureId) {


            $stmt = $vote->getVotesForPicture();

            $votes = array();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);

                $vote = array(
                    "pictureId" => $pictureId,
                    "userId" => $userId,
                    "vote" => $vote,
                    "added" => $added
                );

                array_push($votes, $vote);
            }

            // set response code - 200 OK
            http_response_code(200);

            echo json_encode($votes);
        } else { // response code - 400 bad request
            http_response_code(400);
            echo json_encode(array("message" => "Unable to get votes. Please add pictureId to your params "));
        }
    } catch (Exception $e) {

        http_response_code(401);

        // show error message
        echo json_encode(array(
            "message" => "Access denied.",
            "error" => $e->getMessage()
        ));
    }
}

?>
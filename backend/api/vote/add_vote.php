<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
include_once '../../vendor/firebase/php-jwt/src/BeforeValidException.php';
include_once '../../vendor/firebase/php-jwt/src/ExpiredException.php';
include_once '../../vendor/firebase/php-jwt/src/SignatureInvalidException.php';
include_once '../../vendor/firebase/php-jwt/src/JWT.php';

include_once '../../config/database.php';
include_once '../../config/core.php';
 
include_once '../../objects/vote.php';


use \Firebase\JWT\JWT;
 
$database = new Database();
$db = $database->getConnection();
 
$vote = new Vote($db);
 
$data = json_decode(file_get_contents("php://input"));
// get jwt
$jwt=isset($data->jwt) ? $data->jwt : "";

// if jwt is not empty
if($jwt){
 
    try {
        //Used the JWT key of Google Firebase to decode ID Token of user
        // https://firebase.google.com/docs/auth/admin/verify-id-tokens
        $decoded = JWT::decode($jwt, $key, array('RS256'));
        
        if(
            !empty($data->pictureId) &&
            !empty($data->userId) &&
            !empty($data->vote)
        ){ 
            $vote->pictureId = $data->pictureId;
            $vote->userId = $data->userId;
            $vote->vote = $data->vote;
            $vote->added = date('Y-m-d H:i:s');
         
            if($vote->add()){
                // response code - 201 OK
                http_response_code(201);
                echo json_encode(array("message" => "Vote was added."));
            }
            else{
                // response code - 503 service unavailable
                http_response_code(503);
                echo json_encode(array("message" => "Sorry, something went wrong. Unable to add vote."));
            }
        }
        else{
            // response code - 400 bad request
            http_response_code(400);
            echo json_encode(array("message" => "Unable to add vote. Data is incomplete."));
        }
    } catch (Exception $e){
 
        // set response code - 401 unauthorized
        http_response_code(401);
     
        // show error message
        echo json_encode(array(
            "message" => "Access denied.",
            "error" => $e->getMessage()
        ));
}

} else{
 
    // set response code - 401 unauthorized
    http_response_code(401);
 
    // tell the user access denied
    echo json_encode(array("message" => "Access denied. No JWT provided."));
}
?>
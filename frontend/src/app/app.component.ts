import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth/auth.service';
import { NgxSpinnerService } from "ngx-spinner";
import { SpinnerService } from './services/spinner/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private authService: AuthService, private spinnerService: SpinnerService, private spinner: NgxSpinnerService) {
    this.spinnerService.isLoading.subscribe((isLoading) => {
      if (isLoading) {
        this.spinner.show();
      } else {
        this.spinner.hide();

      }
    });

  }

  signOut() {
    this.authService.signOut();
  }

  isAuthorized(): boolean {
    return this.authService.isAuthorized();
  }
}

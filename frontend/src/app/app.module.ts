import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './components/login/login.component';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment'
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AuthGuard } from './services/auth/auth.guard';
import { AuthService } from './services/auth/auth.service';
import { PhotosService } from './services/photos/photos.service';
import { AlbumOverviewComponent } from './components/album-overview/album-overview.component';
import { ContentThumbComponent } from './components/shared/content-thumb/content-thumb.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Interceptor } from './services/interceptor/interceptor.service';
import { RatingComponent } from './components/rating/rating.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ResultsComponent } from './components/results/results.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { SpinnerService } from './services/spinner/spinner.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AppComponent,
    LoginComponent,
    ContentThumbComponent,
    AlbumOverviewComponent,
    RatingComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSpinnerModule,
    AngularFireModule.initializeApp(environment.firebase),
    FontAwesomeModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    BrowserAnimationsModule
  ],
  providers: [AuthGuard, AuthService, PhotosService, SpinnerService,
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private library: FaIconLibrary) {
    library.addIconPacks(far, fab, fas);
  }
 }

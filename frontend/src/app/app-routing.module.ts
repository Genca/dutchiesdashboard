import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './services/auth/auth.guard';
import { AlbumOverviewComponent } from './components/album-overview/album-overview.component';
import { RatingComponent } from './components/rating/rating.component';
import { ResultsComponent } from './components/results/results.component';


const routes: Routes = [ 
  {
    path: '',
    canActivate:
      [AuthGuard], children: [
        { path: '', component: AlbumOverviewComponent },
        { path: 'rate/:id', component: RatingComponent},
        { path: 'results/:id', component: ResultsComponent}
      ]
  },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

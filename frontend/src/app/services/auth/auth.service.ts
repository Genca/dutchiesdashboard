import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { User } from '../user/user.model';
import { environment } from '../../../environments/environment'
import { BehaviorSubject } from 'rxjs';
import { SpinnerService } from '../spinner/spinner.service';


@Injectable()
export class AuthService {
  public static readonly ACCESS_TOKEN_STORAGE_KEY: string = 'accesToken';
  public static readonly ID_TOKEN_STORAGE_KEY: string = 'idToken';
  private user: User;
  public isIdentificationReady = new BehaviorSubject(false);

  constructor(
    private spinnerService: SpinnerService,
    private firebaseAuth: AngularFireAuth,
    private router: Router) {
    this.loadUser();
  }

  public getUserId() {
    return this.user.email;
  }

  private loadUser() {
    this.isIdentificationReady.next(false);
    this.spinnerService.isLoading.next(true);
    this.firebaseAuth.user.subscribe(user => {
      if (user != null) {
        this.user = user;
      }
      this.isIdentificationReady.next(true);
      this.spinnerService.isLoading.next(false);
    });

  }

  public async signInWithGoogle() {

    let provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('email');
    provider.addScope('profile');
    provider.addScope('https://www.googleapis.com/auth/drive');
    provider.addScope('https://www.googleapis.com/auth/drive.photos.readonly');
    provider.addScope('https://www.googleapis.com/auth/photoslibrary');
    provider.addScope('https://www.googleapis.com/auth/photoslibrary.readonly.appcreateddata');
    provider.addScope('https://www.googleapis.com/auth/photoslibrary.readonly');

    let result = await this.firebaseAuth.auth.signInWithPopup(provider)
    this.signInSuccesHandler(result);
    this.router.navigate(['/']);
  }

  signInSuccesHandler(result) {
    const credential = result.credential as firebase.auth.OAuthCredential;
    sessionStorage.setItem(
      AuthService.ACCESS_TOKEN_STORAGE_KEY, credential.accessToken
    );
    if (this.getIdToken() === null) {
      sessionStorage.setItem(
        AuthService.ID_TOKEN_STORAGE_KEY, credential.idToken
      );
    }
  }

  signOut() {
    this.user = null;
    this.firebaseAuth.auth.signOut();

    sessionStorage.clear()

    this.router.navigate(['login']);
  }

  public isLoggedIn(): boolean {
    return this.user !== null && this.user !== undefined;
  }


  public isAuthorized() {
    return this.isLoggedIn() && environment.authorized.includes(this.user.email);
  }

  public getAccessToken() {
    return sessionStorage.getItem(AuthService.ACCESS_TOKEN_STORAGE_KEY);
  }

  private getIdToken() {
    return sessionStorage.getItem(AuthService.ID_TOKEN_STORAGE_KEY);
  }



  public reAuthenticate() {
    this.signOut();
  }

} 
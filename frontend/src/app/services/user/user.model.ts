import * as firebase from 'firebase/app';

export interface User {
    uid: string;
    email: string;
    photoURL?: string;
    displayName?: string;
    credential?: firebase.auth.OAuthCredential; 
}

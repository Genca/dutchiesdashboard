import { Injectable } from '@angular/core';
import { Vote } from './vote.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VoteService {
  private VOTES_URL = '/votes'
  private PICTURE_ID= 'pictureId';
  private USER_ID= 'userId';
  constructor(private fs: AngularFirestore) {}

  getVotesForPicture(picture: Content): Observable<Vote[]> {
    return this.fs.collection<Vote>(this.VOTES_URL, ref => ref.where(this.PICTURE_ID, '==', picture.id)).valueChanges();
  }

  addVote(vote: Vote) {
    this.fs.collection<Vote>(this.VOTES_URL).add(vote);
  }

}

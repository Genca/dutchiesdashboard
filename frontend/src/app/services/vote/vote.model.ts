export interface Vote {
    pictureId: string;
    userId: string;
    vote: number;
}
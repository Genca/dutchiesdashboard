import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { Observable } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { SpinnerService } from '../spinner/spinner.service';

@Injectable()
export class Interceptor implements HttpInterceptor {
    constructor(private authService: AuthService, private spinnerService: SpinnerService) { }

    addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        return req.clone({ setHeaders: { Authorization: 'Bearer ' + token } });
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.spinnerService.isLoading.next(true);
        return next.handle(this.addToken(req, this.authService.getAccessToken()))
            .pipe(catchError((error) => {
                if (error instanceof HttpErrorResponse
                    && (<HttpErrorResponse>error).status >= 400
                    && (<HttpErrorResponse>error).status < 500) {
                    this.authService.reAuthenticate();
                } else {
                    return Observable.throw(error);
                }
                this.spinnerService.isLoading.next(false);
            }), finalize(() => {
                this.spinnerService.isLoading.next(false);
            })
        )
    }
}
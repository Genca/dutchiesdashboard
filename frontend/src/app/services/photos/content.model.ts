interface Content {
    id: string;
    title: string;
    src: string;
}

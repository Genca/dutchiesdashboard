import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { forkJoin, Observable, throwError } from 'rxjs';
import { error } from 'util';


@Injectable()
export class PhotosService {

  constructor(private http: HttpClient) { }

  getAllAlbums() {
    const ownAlbums: Observable<any> = this.http
      .get('https://photoslibrary.googleapis.com/v1/albums')
    const sharedAlbums: Observable<any> = this.http
      .get('https://photoslibrary.googleapis.com/v1/sharedAlbums')
    return forkJoin(
      ownAlbums,
      sharedAlbums
    ).pipe(
      map(([first, second]) => {
        // combineLatest returns an array of values, here we map those values to an object
        return this.getUnique(this.combinedAlbums(first, second), 'id');
      }), catchError (error => {
        console.error("Error while getting albums: " + error);
        return throwError(error);
      }
      )
    );
  }

  private combinedAlbums(first, second): Content[] {
      return [...first.albums, ...second.sharedAlbums].map(x => {
        const y = {
          id: x.id,
          title: x.title,
          src: x.coverPhotoBaseUrl
        };
        return y;
      }) as Content[];
  }

  private getUnique(arr: Content[], comp: string) {

    const unique = arr
      .map(e => e[comp])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);

    return unique;
  }

  getPhotosFromAlbum(albumId: String): Observable<Content[]> {
    return this.http.post('https://photoslibrary.googleapis.com/v1/mediaItems:search', {
      'pageSize': '100',
      'albumId': albumId
    }).pipe(map(result => {

      let mediaItems = result["mediaItems"];

       return mediaItems.map((item: any)=> {
          return {
            id: item.id,
            title: item.filename,
            src: item.baseUrl
           } as Content;
         });
    }), catchError (error => {
      console.error("Error while getting albums: " + error);
      return throwError(error);
    }))
  }

}

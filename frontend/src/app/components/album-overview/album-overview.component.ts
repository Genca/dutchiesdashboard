import { Component, OnInit } from '@angular/core';
import { PhotosService } from 'src/app/services/photos/photos.service';

@Component({
  selector: 'app-album-overview',
  templateUrl: './album-overview.component.html',
  styleUrls: ['./album-overview.component.css']
})
export class AlbumOverviewComponent implements OnInit {
  albums: Content[] = [];

  constructor(private photoService: PhotosService) { }

  ngOnInit() {
    this.photoService.getAllAlbums().subscribe( 
      result => {
        this.albums = result;
      }, error => {
        console.error("Error while getting albums: " + error);
      }
      );
  }


}

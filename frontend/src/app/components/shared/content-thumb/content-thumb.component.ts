import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { VoteService } from 'src/app/services/vote/vote.service';

@Component({
  selector: 'app-content-thumb',
  templateUrl: './content-thumb.component.html',
  styleUrls: ['./content-thumb.component.css']
})
export class ContentThumbComponent implements OnInit, OnChanges {

  @Input() content: Content;
  @Input() showResults: boolean = false;

  private dislikes: number;
  private nutral: number;
  private likes: number;
  private loves: number;

  constructor(private voteService: VoteService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.showResults) {
      this.voteService.getVotesForPicture(this.content).subscribe(results => {
        this.dislikes = results.filter(result => result.vote === 0).length;
        this.nutral = results.filter(result => result.vote === 1).length;
        this.likes = results.filter(result => result.vote === 2).length;
        this.loves = results.filter(result => result.vote === 3).length;
      }
      );
    }    
  }


}

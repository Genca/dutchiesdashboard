import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentThumbComponent } from './content-thumb.component';

describe('ContentThumbComponent', () => {
  let component: ContentThumbComponent;
  let fixture: ComponentFixture<ContentThumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentThumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentThumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

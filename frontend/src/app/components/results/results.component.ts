import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PhotosService } from 'src/app/services/photos/photos.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  private albumId: string;
  private pictures: Content[];
  constructor(private route: ActivatedRoute, private photoService: PhotosService) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.albumId = params['id'];
        }
      );
      this.photoService.getPhotosFromAlbum(this.albumId).subscribe(result => {
        this.pictures = result;
      });

  }


}

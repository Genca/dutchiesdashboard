import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PhotosService } from 'src/app/services/photos/photos.service';
import { Vote } from 'src/app/services/vote/vote.model';
import { VoteService } from 'src/app/services/vote/vote.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  private albumId: string;
  private shownPicture: Content;
  private pictures: Content[];
  private picturesLoaded = false; 
  private index: number;

  constructor(private route: ActivatedRoute, private photoService: PhotosService, private voteService: VoteService, private authService: AuthService) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.albumId = params['id'];
        }
      );
    this.photoService.getPhotosFromAlbum(this.albumId).subscribe(result => {
      this.pictures = result;
      this.picturesLoaded = true;
      this.showNextPicture();
    });

  }

  private showNextPicture() {
    if (this.index === undefined) {
      this.index = 0;
    } else {
      this.index++;
    }
    if (this.index <= this.pictures.length) {
      this.shownPicture = this.pictures[this.index];
  
    }
  }

  addVote(rating: number) {
    let vote: Vote = {
      pictureId: this.shownPicture.id,
      userId: this.authService.getUserId(),
      vote: rating
    }
    this.voteService.addVote(vote);
    this.showNextPicture();
  }



}

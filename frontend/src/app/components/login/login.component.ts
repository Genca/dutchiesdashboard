import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  authorized = true;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.handleLoggedInUser();
  }

  login() {
    this.authService.signInWithGoogle();
  }

  handleLoggedInUser() {
    this.authService.isIdentificationReady.subscribe(isReady => {

      if (!isReady || !this.authService.isLoggedIn()) return;

      if (this.authService.isAuthorized()) {
        this.router.navigate(["/"]);
      } else {
        this.authorized = false;
      }
    })
  }

}

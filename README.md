# DutchiesDashboard

This project is a *proof of concept* for a voting tool for DutchiesontheGram. Dutchiesonthegam is a instagram account runned by three girls, who lacked a tool for deciding which photos from a photoshoot turned out best.

This is not the final product.

## How it works

- Create an album on Google Photos and share it with the the Google accounts of your fellow voters.
- Log in to the Dutchiesapp
- You will now see an overview of all the albums available to you on Google Photos. You can choose to rate them or view results.

## Includes

- Access only when logged in with authorized Google account. 
- Authentication with Google through Google Firebase. 
- Recovering all (shared) albums from Google photo's. 
- Vote functionality for voting on each photo withing album. 
- Loading screen
- Results of each voting.
- Firebase database for saving vote results.
- Bootstrap styling. 
- RxJs for reactivity. 
- Uses Font-awesome for Icons. 
- Always centered images when voting.


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.4.

